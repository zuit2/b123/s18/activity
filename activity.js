
let trainer = {
	name : "Ash Ketchum",
	age: 14,
	address: {
			city: "Osaka",
			country: "Japan"
	},
	friends: [],
	pokemons:[],
	catch: function (pokemon) {
		// console.log(this.pokemons.length);
		if (this.pokemons.length >= 6){
			// console.log(this.pokemons);
			console.log("A trainer should only have 6 pokemons to carry.");
		}
		else {
			this.pokemons.push(pokemon);
			// console.log(this.pokemons);
			console.log(`Gotcha, ${pokemon}`);
		}
	},
	release: function () {
		console.log(this.pokemons.length);
		if (this.pokemons.length <=0){
			// alert("You have no more pokemons! Catch one first.");
			console.log("You have no more pokemons! Catch one first.");
		}
		else {
			this.pokemons.pop();
			console.log(this.pokemons);
		}
	}

};

console.log(trainer);

function Pokemon (name, type, level) {
	this.name = name;
	this.type = type;
	this.level = level;
	this.hp = 3*this.level;
	this.atk = 2.5*this.level;
	this.def = 2*this.level;
	this.isFainted = false;

	this.tackle = function (pokemon){
		// STRETCH GOAL HERE
		if(!pokemon.isFainted && pokemon.hp>0){
			console.log(`${this.name} tackled ${pokemon.name}!`);
			let origHP = pokemon.hp;
			pokemon.hp -= this.atk;
			// console.log(pokemon.hp);
			if(pokemon.hp<=0){
				pokemon.hp=0; //HP reset
				pokemon.faint();
			};
			console.log(`${this.name} atk (${this.atk}) -> ${pokemon.name} HP (${origHP}) = ${pokemon.name} HP (${pokemon.hp})`);
		}
		else {
			console.log(`${pokemon.name} has already fainted. Please stop.`);
		}
		
		
	},
	this.faint = function (){
		// alert(`${this.name} has fainted.`);
		console.log(`${this.name} has fainted.`);
		this.hp = 0;
		this.isFainted = true;
	}
}

let pokemon1 = new Pokemon("Pikachu","Electric",6);
let pokemon2 = new Pokemon("Charmander","Fire",5);
let pokemon3 = new Pokemon("Bulbasaur","Water",4);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

